Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: selenium
Source: https://github.com/SeleniumHQ/selenium
Files-Excluded:
 *.yml
 *.bzl
 .baz*
 .editorconfig
 .devcontainer
 .git*
 .idea
 .mailmap
 .tours
 BUILD.bazel
 WORKSPACE
 common
 copyiedriver.bat
 cpp
 deploys
 dotnet
 generate_api_docs.sh
 generate_web_code.sh
 go*
 java*
 package.json
 package-lock.json
 private
 py/.tours
 rake*
 Rakefile
 rb
 scripts
 selenium.iml
 sonar-project.properties
 third_party
 tours
Comment: The original upstream tarball includes various files and data for
 programming languages and platforms which aren't needed for the packaging of
 python-selenium. Due this all folders and files like prebuild and minimized
 files, config setups for IDEs and platform specific files for Windows got
 excluded.

Files: *
Copyright: 2007-2011 David Burns
           2007-2009 Google Inc.
           2007-2024 WebDriver committers
License: Apache-2.0

Files: debian/*
Copyright: 2011 Sascha Girrulat <sascha@girrulat.de>
 2022-2024 Carsten Schoenert <c.schoenert@t-online.de>
License: Apache-2.0

Files: docs/*
Copyright: 2009-2023 Software Freedom Conservancy
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".
